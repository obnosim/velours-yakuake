# Velours Yakuake

Dark, monochromatic and translucent skin for Yakuake that is designed to integrate perfectly with the [Plasma theme](https://gitlab.com/obnosim/velours) of the same name.

For better results:
- make sure to set Yakuake's background color opacity to 0%
- use console.png, found at the root of the package, as the background image of the terminal. Set it in the Appearance tab of the profile configuration window, and set the background opacity to 0%.
